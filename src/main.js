// IMPORTAMOS LA CREACION DE LA APP
import app from './config/configApp';
// IMPORTAMOS LOS ARCHIVOS DE ESTADOS
import store from '@/vuex';
// IMPORTAMOS LA LIBRERIA DE DISEÑO JUNTO CON SUS ESTILOS CSS
import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css'

app.config.productionTip = false;

app.use(Antd)
app.use(store);
app.mount('#app');
