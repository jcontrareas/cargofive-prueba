import { createStore } from 'vuex'

// INPORTANDO LA CONFIGURACION DE AXIOS EN DONDE SE ENCUENTRA EL ENDPOIND DE ORIGEN DE LOS DATOS
import { DataService } from '@/config/dataService/dataService'

export default createStore({
  state: {
    ports: [],
    // INICIAMOS LOS VALORES DE PAGINADO 
    meta: {
      current_page: 1,
      per_page: 100,
      total: 0,
    },
    countries: '',

  },
  getters: {
    ports: (state) => state.ports,
    meta: (state) => state.meta,
    countries: (state) => state.countries

  },
  mutations: {
    // FUNCION PARA SEPARAR LOS DATOS DEL PAGINATE, ASI COMO PARA REALIZAR LOS FILTRADOS POR NOMBRE, PAIS Y CONTINENTE
    SET_PORTS(state, { data, filters }) {
      // VERIFICAMOS SI EN EL OBJETO DE FILTERS EXISTE LA VARIABLE Y REALIZAMOS EL FILTRADO
      state.ports = data.data
      if (filters?.country) {
        state.ports = data.data.filter((item) => item.country == filters.country)
      }
      if (filters?.continent) {
        state.ports = data.data.filter((item) => item.continent == filters.continent)
      }
      if (filters?.name) {
        state.ports = data.data.filter((item) => {
          return filters.name.toLowerCase().split(' ').every(v => item.name.toLowerCase().includes(v))
        })
      }
      state.meta = data.meta
    }
  },
  actions: {
    // FUNCION PARA LISTAR TODOS LOS PUERTOS 
    async list({ commit }, params = {}) {
      try {
        const data = (await DataService.get('api/ports', params)).data
        commit('SET_PORTS', { data, filters: params.filters })
        return data
      } catch (error) {
        return error
      }
    },
  },
  modules: {
  }
})
