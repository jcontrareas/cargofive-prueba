import axios from 'axios'
import { notification } from 'ant-design-vue'

// IMPORTAMOS Y CREAMOS LA CONEXION DE AXIOS CON EL ENDPOIND
const API_ENDPOINT = process.env.VUE_APP_API_ENDPOINT
const client = axios.create({
  baseURL: API_ENDPOINT,
})
class DataService {
  static get(path = '', params = {}) {
    return client.get(path, { params })
  }
}

// INTERCEPTAMOS TODAS LAS RESPUESTAS QUE CONTENGA ALGUN ERROR Y PODER MOSTRAR LA NOTIFICACION REQUERIA DE ACUERDO AL CODIGO
client.interceptors.response.use(
  (response) => response,
  (error) => {
    const { response } = error
    const originalRequest = error.config
    if (response) {
      if (response.status === 500) {
        notification.error({
          message: '¡Upss!',
          description: response.data.message
        })
      } else if (response.status === 422) {
        const {
          data: { errors }
        } = response
        Object.values(errors).forEach((error) => {
          notification.error({
            message: '¡Upss!',
            description: error
          })
        })
      } else {
        return originalRequest
      }
    }
    return Promise.reject(error)
  }
)
export { DataService }
