// CREACION DE LA APLICACION VUEJS Y EXPORTACION
import { createApp } from 'vue';
import App from '../App.vue';

const app = createApp(App);

export default app;
