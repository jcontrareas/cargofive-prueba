// ARRAY DE OBJETOS CON TODOS LOS CONTINENTES
const CONTINENT = [
  {
    id: 'AF',
    name: 'AF - África'
  },
  {
    id: 'AN',
    name: 'AN - Antártida'
  },
  {
    id: 'AS',
    name: 'AS - Asia'
  },
  {
    id: 'OC',
    name: 'OC - Oceanía'
  },
  {
    id: 'EU',
    name: 'EU - Europa'
  },
  {
    id: 'NA',
    name: 'NA - América del Norte'
  },
  {
    id: 'SA',
    name: 'SA - América del Sur'
  }
]

export { CONTINENT }
